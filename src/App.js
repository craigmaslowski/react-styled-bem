import React from 'react';
import { Route, Switch } from 'react-router';
import { BrowserRouter as Router } from 'react-router-dom';

import Page from './components/Page';
import SiteNav from './components/SiteNav';

import Bonus from './components/BonusDemo';
import ButtonsOnly from './components/ButtonsOnlyDemo';
import StyledComponents from './components/StyledComponentsDemo';
import Modifiers from './components/StyledComponentsModifiersDemo';
import Reakit from './components/ReakitDemo';

const App = props => (
  <Router>
    <Page>
      <Page.Header>
        <SiteNav />
      </Page.Header>
      <Page.Content>
        <Switch>
          <Route exact path="/styled-components" component={StyledComponents} />
          <Route path="/styled-components-modifiers" component={Modifiers} />
          <Route path="/reakit" component={Reakit} />
          <Route path="/bonus" component={Bonus} />
          <Route path="/" component={ButtonsOnly} />
        </Switch>
      </Page.Content>
    </Page>
  </Router>
);

export default App;
