import React from 'react';
import {
  reakitUsage as usage, reakitComponent as component
} from './code';
import Example from './Example';

import Button from './ReakitButton';
const { Danger, Large, Primary, Secondary } = Button;

const ReakitDemo = prop => (
  <Example>
    <Example.Demo>
      <Example.Heading>Demo</Example.Heading>

      <Button>Default</Button>
      <Button as={Danger}>Danger</Button>
      <Button as={Primary}>Primary</Button>
      <Button as={Secondary}>Secondary</Button>
      <Button as={Large}>Large</Button>
      <Button as={[Large, Danger]}>Large Danger</Button>
      <Button as={[Large, Primary]}>Large Primary</Button>
      <Button as={[Large, Secondary]}>Large Secondary</Button>
      <Button as={[Danger, Primary]}>
        Problem: Danger or Primary?
      </Button>

      <Example.Code>{usage}</Example.Code>
    </Example.Demo>

    <Example.Components>
      <Example.Heading>Button using Reakit</Example.Heading>
      <Example.Code>{component}</Example.Code>
    </Example.Components>
  </Example>
);

export default ReakitDemo;
