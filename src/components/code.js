/*
 *
 * styled-components
 *
 */
export const styledComponentsUsage =`<Button>Default</Button>
<Button.Large>Large</Button.Large>
<Button.Primary>Primary</Button.Primary>
<Button.LargePrimary>Large Primary</Button.LargePrimary>
<Button.Secondary>Secondary</Button.Secondary>
<Button.LargeSecondary>Large Secondary</Button.LargeSecondary>
<Button.Danger>Danger</Button.Danger>
<Button.LargeDanger>Large Danger</Button.LargeDanger>`;

 /* eslint-disable no-template-curly-in-string */
export const styledComponentsComponent = `import styled, { css } from 'styled-components';

const Button = styled.button${'`'}
  background: #eceff5;
  border: 0;
  border-radius: 4px;
  color: #05090e;
  cursor: pointer;
  font-size: 1rem;
  padding: 10px;
  margin: 10px;
  text-transform: uppercase;
${'`'};

const large = css${'`'}
  font-size: 125%;
${'`'}
const primary = css${'`'}
  background: #345995;
  color: #fff;
${'`'};

const secondary = css${'`'}
  background: #03CEA4;
  color: #fff;
${'`'};

const danger = css${'`'}
  background: #CA1551;
  color: #fff;
${'`'};

Button.Large = styled(Button)${'`'}
  ${'${large}'}
${'`'};

Button.Danger = styled(Button)${'`'}
  ${'${danger}'}
${'`'};

Button.Primary = styled(Button)${'`'}
  ${'${primary}'}
${'`'};

Button.Secondary = styled(Button)${'`'}
  ${'${secondary}'}
${'`'};

Button.LargeDanger = styled(Button.Danger)${'`'}
  ${'${large}'}
${'`'};

Button.LargePrimary = styled(Button.Primary)${'`'}
  ${'${large}'}
${'`'};

Button.LargeSecondary = styled(Button.Secondary)${'`'}
  ${'${large}'}
${'`'};

export default Button;
 `;
 /* eslint-enable no-template-curly-in-string */


/*
 *
 * styled-components-modifiers
 *
 */

 export const styledComponentsModifiersUsage = `<Button>Default</Button>
<Button modifiers={['danger']}>Danger</Button>
<Button modifiers={['primary']}>Primary</Button>
<Button modifiers={['secondary']}>Secondary</Button>
<Button modifiers={['large']}>Large</Button>
<Button modifiers={['large', 'danger']}>Large Danger</Button>
<Button modifiers={['large', 'primary']}>Large Primary</Button>
<Button modifiers={['large', 'secondary']}>Large Secondary</Button>
<Button modifiers={['danger', 'primary']}>
  Problem: Danger or Primary?
</Button>`;

 /* eslint-disable no-template-curly-in-string */
 export const styledComponentsModifiersComponent =
 `import styled from 'styled-components';
import {
  applyStyleModifiers, styleModifierPropTypes
} from 'styled-components-modifiers';

const modifiers = {
  large: () => ${'`'}
    font-size: 125%;
  ${'`'},
  primary: () => ${'`'}
    background: #345995;
    color: #fff;
  ${'`'},
  secondary: () => ${'`'}
    background: #03CEA4;
    color: #fff;
  ${'`'},
  danger: () => ${'`'}
    background: #CA1551;
    color: #fff;
  ${'`'}
};

const Button = styled.button${'`'}
  background: #eceff5;
  border: 0;
  border-radius: 4px;
  color: #05090e;
  cursor: pointer;
  font-size: 1rem;
  padding: 10px;
  margin: 10px;
  text-transform: uppercase;

  ${'${applyStyleModifiers(modifiers)}'}
${'`'};

Button.propTypes = {
  modifiers: styleModifierPropTypes(modifiers)
}

export default Button;`;
/* eslint-enable no-template-curly-in-string */


/*
 *
 * Reakit
 *
 */

export const reakitUsage = `const { Danger, Large, Primary, Secondary } = Button;

<Button>Default</Button>
<Button as={Danger}>Danger</Button>
<Button as={Primary}>Primary</Button>
<Button as={Secondary}>Secondary</Button>
<Button as={Large}>Large</Button>
<Button as={[Large, Danger]}>Large Danger</Button>
<Button as={[Large, Primary]}>Large Primary</Button>
<Button as={[Large, Secondary]}>Large Secondary</Button>
<Button as={[Danger, Primary]}>
  Problem: Danger or Primary?
</Button>`;

export const reakitComponent = `
import { styled, Button as ButtonBase } from 'reakit';

const Button = styled(ButtonBase)${'`'}
  background: #eceff5;
  border: 0;
  border-radius: 4px;
  color: #05090e;
  cursor: pointer;
  font-size: 1rem;
  padding: 10px;
  margin: 10px;
  text-transform: uppercase;
${'`'};

Button.Danger = styled(Button)${'`'}
  background: #CA1551;
  color: #fff;
${'`'};

Button.Large = styled(Button)${'`'}
  font-size: 125%;
${'`'};

Button.Primary = styled(Button)${'`'}
  background: #345995;
  color: #fff;
${'`'};

Button.Secondary = styled(Button)${'`'}
  background: #03CEA4;
  color: #fff;
${'`'};

export default Button;
`;

/*
 *
 * Example
 *
 */

export const exampleUsage = `<Example>
  <Example.Demo>
    <Example.Heading>Page</Example.Heading>

    <Page>
      <Page.Header><h1>Header</h1></Page.Header>
      <Page.Content>Content</Page.Content>
    </Page>

    <Example.Code>{usage}</Example.Code>
  </Example.Demo>

  <Example.Components>
    <Example.Heading>Button using styled-components</Example.Heading>
    <Example.Code>{component}</Example.Code>
  </Example.Components>
</Example>`;

export const exampleComponent = `import styled from 'styled-components';
import Block from './Block';

const Example = styled(Block)${'`'}
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: auto;
  grid-template-areas:
    "components"
    "demo";

  @media (min-width: 1200px) {
    grid-template-columns: 50% 50%;
    grid-template-rows: auto;
    grid-template-areas:
      "components demo";
  }
${'`'};

Example.Demo = styled(Block)${'`'}
  grid-area: demo;
${'`'};

Example.Components = styled(Block)${'`'}
  grid-area: components;
${'`'};

Example.Heading = styled.h1${'`'}
  color: #132137;
${'`'};

Example.Code = styled.pre${'`'}
  background: #eceff5;
  border: solid 1px #dae0eb;
  border-radius: 4px;
  font-family: monospace;
  padding: 20px;
${'`'};

export default Example;`;

/*
 *
 * Page
 *
 */
export const pageUsage = `<Page>
  <Page.Header><h1>Heading</h1></Page.Header>
  <Page.Content>Content</Page.Content>
</Page>`;

export const pageComponent = `import styled from 'styled-components';

const Page = styled.div${'`'}
  display: grid;
  grid-template-areas:
    "header"
    "content"
${'`'};

Page.Header = styled.div${'`'}
  grid-area: header;
  background: #345995;
${'`'};

Page.Content = styled.div${'`'}
  grid-area: content;
  height: 100%;
${'`'};

export default Page;`;
