import styled, { css } from 'styled-components';

const Button = styled.button`
  background: #eceff5;
  border: 0;
  border-radius: 4px;
  color: #05090e;
  cursor: pointer;
  font-size: 1rem;
  padding: 10px;
  margin: 10px;
  text-transform: uppercase;
`;

const large = css`
  font-size: 125%;
`
const primary = css`
  background: #345995;
  color: #fff;
`;

const secondary = css`
  background: #03CEA4;
  color: #fff;
`;

const danger = css`
  background: #CA1551;
  color: #fff;
`;

Button.Large = styled(Button)`
  ${large}
`;

Button.Danger = styled(Button)`
  ${danger}
`;

Button.Primary = styled(Button)`
  ${primary}
`;

Button.Secondary = styled(Button)`
  ${secondary}
`;

Button.LargeDanger = styled(Button.Danger)`
  ${large}
`;

Button.LargePrimary = styled(Button.Primary)`
  ${large}
`;

Button.LargeSecondary = styled(Button.Secondary)`
  ${large}
`;

export default Button;
