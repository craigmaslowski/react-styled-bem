import React from 'react';
import {
  styledComponentsModifiersUsage as usage,
  styledComponentsModifiersComponent as component
} from './code';
import Example from './Example';

import Button from './StyledComponentsModifiersButton';

const StyledComponentsModifiersDemo = prop => (
  <Example>
    <Example.Demo>
      <Example.Heading>Demo</Example.Heading>

      <Button>Default</Button>
      <Button modifiers={['danger']}>Danger</Button>
      <Button modifiers={['primary']}>Primary</Button>
      <Button modifiers={['secondary']}>Secondary</Button>
      <Button modifiers={['large']}>Large</Button>
      <Button modifiers={['large', 'danger']}>Large Danger</Button>
      <Button modifiers={['large', 'primary']}>Large Primary</Button>
      <Button modifiers={['large', 'secondary']}>Large Secondary</Button>
      <Button modifiers={['danger', 'primary']}>
        Problem: Danger or Primary?
      </Button>
      <Example.Code>{usage}</Example.Code>
    </Example.Demo>

    <Example.Components>
      <Example.Heading>Button using styled-components-modifiers</Example.Heading>
      <Example.Code>{component}</Example.Code>
    </Example.Components>
  </Example>
);


export default StyledComponentsModifiersDemo;
