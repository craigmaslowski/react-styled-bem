import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import styled from 'styled-components';
import Block from './Block';

const Link = styled(RouterLink)`
  color: #eceff5;
  margin: 10px;
  text-decoration: none;

  &:hover {
    text-decoration: underline;
  }

  &:visited {
    color: #eceff5;
  }
`;

const Nav = styled(Block)`
  box-shadow: 0 4px 2px -2px rgba(0, 0, 0, 0.2);
  height: 68px;
`;

const NavHeading = styled.h1`
  color: #eceff5;
  display: inline-block;
`;

const NavItem = styled.div`
  align-items: center;
  display: inline-flex;
  font-size: 1.25rem;
  min-height: 100%;
  justify-content: center;
`;

const SiteNav = () => (
  <Nav>
    <NavHeading>BEM Component Approaches</NavHeading>
    <NavItem>
      <Link to="/styled-components">Styled-Components</Link>
    </NavItem>
    <NavItem>
      <Link to="/styled-components-modifiers">Styled-Components-Modifiers</Link>
    </NavItem>
    <NavItem>
      <Link to="/reakit">Reakit</Link>
    </NavItem>
    <NavItem>
      <Link to="/bonus">Bonus</Link>
    </NavItem>
  </Nav>
);

export default SiteNav;
