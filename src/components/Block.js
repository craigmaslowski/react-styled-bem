import styled from 'styled-components';

const Block = styled.div`
  padding: 10px 20px;
`;

export default Block;
