import styled from 'styled-components';
import {
  applyStyleModifiers, styleModifierPropTypes
} from 'styled-components-modifiers';

const modifiers = {
  large: () => `
    font-size: 125%;
  `,
  primary: () => `
    background: #345995;
    color: #fff;
  `,
  secondary: () => `
    background: #03CEA4;
    color: #fff;
  `,
  danger: () => `
    background: #CA1551;
    color: #fff;
  `
};

const Button = styled.button`
  background: #eceff5;
  border: 0;
  border-radius: 4px;
  color: #05090e;
  cursor: pointer;
  font-size: 1rem;
  padding: 10px;
  margin: 10px;
  text-transform: uppercase;

  ${applyStyleModifiers(modifiers)}
`;

Button.propTypes = {
  modifiers: styleModifierPropTypes(modifiers)
}

export default Button;
