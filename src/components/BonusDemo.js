import React from 'react';
import {
  exampleUsage, exampleComponent, pageUsage, pageComponent
} from './code';
import Example from './Example';
import Page from './Page';

const StyledComponentsDemo = prop => (
  <React.Fragment>
    <Example>
      <Example.Demo>
        <Example.Heading>Demo</Example.Heading>

        <Page>
          <Page.Header><h1>Heading</h1></Page.Header>
          <Page.Content>Content</Page.Content>
        </Page>

        <Example.Code>{pageUsage}</Example.Code>
      </Example.Demo>

      <Example.Components>
        <Example.Heading>Page</Example.Heading>
        <Example.Code>{pageComponent}</Example.Code>
      </Example.Components>
    </Example>
    <hr />
    <Example>
      <Example.Demo>
        <Example.Heading>Demo</Example.Heading>

        <h3>You're looking at one.</h3>
        <p>Below is the code used to render the Page example above.</p>

        <Example.Code>{exampleUsage}</Example.Code>
      </Example.Demo>

      <Example.Components>
        <Example.Heading>Example</Example.Heading>
        <Example.Code>{exampleComponent}</Example.Code>
      </Example.Components>
    </Example>
  </React.Fragment>
);

export default StyledComponentsDemo;
