import styled from 'styled-components';
import Block from './Block';

const Example = styled(Block)`
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: auto;
  grid-template-areas:
    "components"
    "demo";

  @media (min-width: 1200px) {
    grid-template-columns: 50% 50%;
    grid-template-rows: auto;
    grid-template-areas:
      "components demo";
  }
`;

Example.Demo = styled(Block)`
  grid-area: demo;
`;

Example.Components = styled(Block)`
  grid-area: components;
`;

Example.Heading = styled.h1`
  color: #132137;
`;

Example.Code = styled.pre`
  background: #eceff5;
  border: solid 1px #dae0eb;
  border-radius: 4px;
  font-family: monospace;
  padding: 20px;
`;

export default Example;
