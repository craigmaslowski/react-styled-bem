import { styled, Button as ButtonBase } from 'reakit';

const Button = styled(ButtonBase)`
  background: #eceff5;
  border: 0;
  border-radius: 4px;
  color: #05090e;
  cursor: pointer;
  font-size: 1rem;
  padding: 10px;
  margin: 10px;
  text-transform: uppercase;
`;

Button.Danger = styled(Button)`
  background: #CA1551;
  color: #fff;
`;

Button.Large = styled(Button)`
  font-size: 125%;
`;

Button.Primary = styled(Button)`
  background: #345995;
  color: #fff;
`;

Button.Secondary = styled(Button)`
  background: #03CEA4;
  color: #fff;
`;

export default Button;
