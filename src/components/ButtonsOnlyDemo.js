import React from 'react';
import styled from 'styled-components';
import Button from './ReakitButton';

const CenterContent = styled.div`
  padding-top: 100px;
  text-align: center;
  width: 100%;
`;

const Container = styled.div`
  margin-bottom: 40px;
`;

const { Danger, Large, Primary, Secondary } = Button;

const StyledComponents = prop => (
    <CenterContent>
      <Container>
        <Button>Default</Button>
        <Button as={Danger}>Danger</Button>
        <Button as={Primary}>Primary</Button>
        <Button as={Secondary}>Secondary</Button>
      </Container>
      <Container>
        <Button as={Large}>Large</Button>
        <Button as={[Large, Danger]}>Large Danger</Button>
        <Button as={[Large, Primary]}>Large Primary</Button>
        <Button as={[Large, Secondary]}>Large Secondary</Button>
      </Container>
    </CenterContent>
);

export default StyledComponents;
