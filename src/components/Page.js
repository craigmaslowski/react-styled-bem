import styled from 'styled-components';

const Page = styled.div`
  display: grid;
  grid-template-areas:
    "header"
    "content"
`;

Page.Header = styled.div`
  grid-area: header;
  background: #345995;
`;

Page.Content = styled.div`
  grid-area: content;
  height: 100%;
`;

export default Page;
