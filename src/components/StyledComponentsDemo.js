import React from 'react';
import {
  styledComponentsUsage as usage,  styledComponentsComponent as component
} from './code';
import Example from './Example';

import Button from './StyledComponentsButton';

const StyledComponentsDemo = prop => (
  <Example>
    <Example.Demo>
      <Example.Heading>Demo</Example.Heading>

      <Button>Default</Button>
      <Button.Danger>Danger</Button.Danger>
      <Button.Primary>Primary</Button.Primary>
      <Button.Secondary>Secondary</Button.Secondary>
      <Button.Large>Large</Button.Large>
      <Button.LargeDanger>Large Danger</Button.LargeDanger>
      <Button.LargePrimary>Large Primary</Button.LargePrimary>
      <Button.LargeSecondary>Large Secondary</Button.LargeSecondary>

      <Example.Code>{usage}</Example.Code>
    </Example.Demo>

    <Example.Components>
      <Example.Heading>Button using styled-components</Example.Heading>
      <Example.Code>{component}</Example.Code>
    </Example.Components>
  </Example>
);

export default StyledComponentsDemo;
