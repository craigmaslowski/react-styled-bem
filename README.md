# Three approaches to BEM components in React with styled-components

This repo was built to accompany a presentation exploring three approaches for making BEM components in React using styled-components, styled-components-modifiers, and Reakit.

More info:

- [BEM](https://en.bem.info)
- [React Components](https://reactjs.org/docs/components-and-props.html)
- [Presentational vs Container Components](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)

Libraries:

- [styled-components](https://styled-components.com) - [github](https://github.com/styled-components/styled-components)
- [Reakit](https://reakit.io) - [github](https://github.com/reakit/reakit)
- [styled-components-modifiers](https://github.com/decisiv/styled-components-modifiers)
- [styled-tools](https://github.com/diegohaz/styled-tools)
- [polished](https://polished.js.org) - [github](https://github.com/styled-components/polished)
